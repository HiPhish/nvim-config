#########################
 My Neovim configuration
#########################

This is my Neovim configuration. I have licensed it under the MIT-license, but
it's really a "do whatever you want with this code" type of deal, I don't care.
Maybe there is something useful for you, but this repo is mostly just a backup
for me personally. I have tried to explain everything through comments, but
those comments are mainly for me, so your mileage may vary.


Icons
#####

This config uses various private-area code points. To display them properly
install the `codicons`_ font, or even better `neovim_codicons`_.


.. _codicons: https://github.com/microsoft/vscode-codicons
.. _neovim-codicons: https://github.com/ChristianChiarulli/neovim-codicons
