;; extends

(("in"   @TSOperator) (#set! conceal "∈"))
(("<="   @TSOperator) (#set! conceal "≤"))
((">="   @TSOperator) (#set! conceal "≥"))
(("<-"   @TSOperator) (#set! conceal "←"))
(("->"   @TSOperator) (#set! conceal "→"))
; (("=="   @TSOperator) (#set! conceal "≅"))
; (("==="  @TSOperator) (#set! conceal "≡"))


;; ----------------------------------------------------------------------------
; vim: ft=query
