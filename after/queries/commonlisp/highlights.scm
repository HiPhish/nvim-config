;; extends

;; --- [ MORE TERSE OPERATORS ]------------------------------------------------
(((defun_header
    keyword:(defun_keyword) @TSFuncMacro
    (#eq? @TSFuncMacro "lambda")))
 (#set! conceal "λ"))


;; ----------------------------------------------------------------------------
; vim: ft=query
