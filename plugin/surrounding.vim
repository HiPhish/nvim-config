" rhysd/vim-operator-surround
nmap <silent> sa <Plug>(operator-surround-append)
vmap <silent> sa <Plug>(operator-surround-append)
nmap <silent> sd <Plug>(operator-surround-delete)
nmap <silent> sc <Plug>(operator-surround-replace)
