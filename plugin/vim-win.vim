let g:win_resize_height = 1
let g:win_resize_width = 1
let g:win_ext_command_map = {
	\ 'q': 'quit',
	\ 'Q': 'quit!',
	\ 'H': 'wincmd >',
	\ 'J': 'wincmd +',
	\ 'K': 'wincmd -',
	\ 'L': 'wincmd <',
	\ '=': 'wincmd =',
\}
