.. default-role:: code


This directory contains custom overrides for existing colour schemes.  Each
override is registered via auto commands.  Create a separate override file per
colour scheme.
