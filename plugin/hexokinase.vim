let g:Hexokinase_virtualText = ''
let g:Hexokinase_ftAutoload = ['vim', 'css', 'conf', 'zathura']
let g:Hexokinase_optInPatterns = ['full_hex', 'triple_hex', 'rgb', 'rgba']
