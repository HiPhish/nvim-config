--- Settings suitable for Löve2D game development
local M = {
	Lua = {
		runtime = {
			version = 'LuaJIT'
		},
		workspace = {
			library = {'${3rd}/love2d/library'}
		}
	}
}

return M
